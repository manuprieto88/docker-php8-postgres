# docker-php8-postgres

This a docker for PHP developers
=============

Based on ubuntu:focal image

Customization:
- Nginx: 1.19.10
- PHP: 8.0.8
- Node: 16.4.1
- Npm: 7.19.1
- Composer: 2.0.13


The environment is created using: 
- Dockerfile 
- docker-compose.yml
- .env


Building
=============

Linux check user and group id using command: id

### Build using Docker-Compose ###

- Docker compose will use .env file and Dockerfile to create containers

- Change your environment variables in .env file

- docker-compose build

- docker-compose up


### Build image using Docker build ###

Execute next command to create image with name and tag nginx-php8-postgres:php8

- docker build --no-cache -t nginx-php8-postgres:php8 . --build-arg UID=YOUR_USER_ID --build-arg GID=YOUR_GROUP_ID


Building process
-----------------

You can follow building process reading Dockerfile

There are two config files that are going to be copied:

### default.conf ###

For NGINX server configuration. Copied to /etc/nginx/conf.d/default.conf
You can change the default folder used by nginx
After changes use service nginx restart

### supervisord.conf ###

Supervisor is used as a monitor. Basically is used for the service to not to stop.

Connecting to services
----------------------

### Postgres Database ###
docker exec -i dockerdevenvironment_postgres_db_1 psql -U ENV_USERNAME

PSQL Resources:
https://www.datacamp.com/community/tutorials/10-command-line-utilities-postgresql

### Webserver ###
docker exec -ti dockerdevenvironment_nginx_web_1 bash

NGINX Resources:
https://www.linode.com/docs/guides/how-to-configure-nginx/


